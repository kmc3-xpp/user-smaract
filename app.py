#!/usr/bin/python3

import SmarAct as sa

import logging, asyncio, time
from os import environ

from caproto.server import pvproperty, PVGroup
from caproto.asyncio.server import Context

smaract_poll_period = environ.get('SMARACT_POLL_PERIOD', 0.2)

class MotorControllerIOC(PVGroup):
    '''
    Responsible for a single motor controller (SartAct uses two).
    '''
    HOME      = pvproperty(value=0)
    THETA_STATUS    = pvproperty(value=0)
    THETA     = pvproperty(value=0.0)
    DMOV      = pvproperty(value=1)
    THETA_RBV = pvproperty(value=0.0)

    def __init__(self, prefix, host, stage_dict=None, ctrl_dict=None):
        super().__init__(prefix)
        self.prefix = prefix
        self.host = host

        if stage_dict is None:
            stage_dict = {}

        if ctrl_dict is None:
            ctrl_dict = {}

        self.ctrl = sa.IPController(host, **ctrl_dict)
        self.ctrl.get_config()

        self.stage = sa.SmarActStage(
            Controller=self.ctrl,
            Name="theta",
            Channel=0,
            LowerLimit=-3600000000,
            UpperLimit=3600000000,
            **stage_dict)

    async def initialize(self):
        await self.stage.initialize()

    @THETA.putter
    async def THETA(self, instance, value):
        await self.DMOV.write(0)
        await self.stage.asyncSetUserPosition(value)


    @THETA.scan(period=smaract_poll_period, stop_on_error=True)
    async def THETA(self, instance, asynclib):
        try:
            # this should be ASYNC
            t = await self.stage.asyncGetUserPosition()
            if self.THETA_RBV.value != t:
                await self.THETA_RBV.write(t)
        except Exception as e:
            logging.error(f'Error while polling status: {str(e)}')
            raise


    @THETA_STATUS.scan(period=smaract_poll_period, stop_on_error=True)
    async def THETA_STATUS(self, instance, asynclib):
        try:
            # this should be ASYNC
            #s = await self.stage.read_controller_state()
            s = 0
            logging.debug(f's is {s}')
            if s != 0:
                s = 2
            if self.THETA_STATUS.value != 0:
                logging.debug(f'{self.prefix}:THETA_STATUS: {s}')
                await self.THETA_STATUS.write(s)

            done_moving = (s == 0)
            if done_moving != self.DMOV.value:
                await self.DMOV.write(done_moving)

            if done_moving and self.HOME.value == 1:
                logging.info(f'Done homing: s={s}')
                await self.HOME.write(0)

        except Exception as e:
            logging.error(f'Error while polling status: {str(e)}')
            raise

    @HOME.putter
    async def HOME(self, instance, value):
        if (value == 1):
            await self.stage.asyncHome()
            await asyncio.sleep(1)
        else:
            logging.error(f'Apparently you aren\'t supposed to write 0 to ...:HOME')


class SmarActIOC(PVGroup):

    def __init__(self, prefix, air_host=None, vac_host=None):
        '''
        Args:
            prefix: EPICS prefix
            air: air controller host
            vac: vac controller host
	'''

        logging.info(f'Starting IOC on {prefix}')
        super().__init__(prefix=prefix)

        logging.info(f'Initializing controllers')

        air_host = air_host or environ.get('SMARACT_AIR_CTRL_HOST', '172.16.58.201')
        vac_host = vac_host or environ.get('SMARACT_AIR_CTRL_HOST', '172.16.58.200')

        self.air = MotorControllerIOC(prefix+"AIR:", air_host)

        self.vac = MotorControllerIOC(prefix+"VAC:", vac_host,
                                      stage_dict={
                                        'vac': True ,
                                      })

    async def initialize(self):
        await self.air.initialize()
        await self.vac.initialize()

    @property
    def full_pvdb(self):
        db = {}
        db.update(self.pvdb)
        db.update(self.air.pvdb)
        db.update(self.vac.pvdb)
        return db


class Application:

    def __init__(self):

        logging.info('Initializing')

        self.prefix = environ.get('SMARACT_EPICS_PREFIX', 'KMC3:XPP:SMARACT:')

        self.ioc = SmarActIOC(prefix=self.prefix)

    async def run(self):
        logging.info(f'Have PVs:')
        for pv in self.ioc.full_pvdb:
            logging.info(f'  {pv}')

        await self.ioc.initialize()

        self.ctx = Context(self.ioc.full_pvdb)
        task = asyncio.create_task(self.ctx.run())

        while True:
            await asyncio.sleep(0.1)


def main():
    app = Application();
    asyncio.run(app.run());


if __name__ == "__main__":

    logging.basicConfig(level=environ.get('SMARACT_LOG_LEVEL', 'INFO'))
    main()
