FROM registry.fedoraproject.org/fedora-toolbox:38
# requires SMARACT_AIR_CTRL_HOST and SMARACT_VAC_CTRL_HOST as variables

RUN dnf -y install \
    python3-pip \
    python3-numpy \
    PyQt5 \
    && \
    pip install caproto \
    && \
    mkdir -p /opt/smaract/Hardware

RUN adduser -u 9999 cthulhu

# This may appear as a somewhat brutal act...
COPY SmarAct.py /opt/smaract
COPY Hardware/Motor.py /opt/smaract/Hardware/
COPY app.py /opt/smaract/app.py

USER cthulhu
CMD /opt/smaract/app.py
