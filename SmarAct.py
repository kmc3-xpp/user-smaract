#!/usr/bin/python3

import sys, time, socket
#import smaract.ctl as ctl
from Hardware.Motor import Motor

from os import environ

import logging, asyncio

# Uses environment variables:

SMARACT_AIR_CTRL_HOST=environ.get("SMARACT_AIR_CTRL_HOST", "192.168.1.25")
SMARACT_VAC_CTRL_HOST=environ.get("SMARACT_VAC_CTRL_HOST", "192.168.1.200")

class SmarActAxis():
    def __init__(self):
        self.type = None
        self.LL = None
        self.HL = None
        self.safedir = 0
        self.offset = 0
        self.position = 0
        self.encoder = 25

class IPController:
    def __init__(self, address=SMARACT_AIR_CTRL_HOST, port=5000, axis=0 ):
        self.LL = 0
        self.HL = 90
        #time.sleep(0.1)
        self.proto = None  # can be UART or IP
        self.address = ''
        self.port = ''
        self.axis = []  # names come here
        self.axis_num = {}
        self.axis_type = []
        self.axis_sensor = {}

        self.numchan = 0
        self.s = None
        self.time = 0.0
        self.answer = ""
        self.error = 0

        self.errorcode = {}
        self.errorcode[1] = "Syntax"
        self.errorcode[4] = "Parse"
        self.errorcode[7] = "Invalid Parameter"
        self.errorcode[129] = "No Sensor Present"
        #connect
        self.address = address
        self.s = socket.socket()
        self.port = port
        self.axis = axis
        self.proto = "TCPIP"
        self.s.settimeout(5.0)

        try:
            self.s.connect((self.address, self.port))
        except Exception as e:
            logging.error(f"Cannot open: {self.address}:{self.port}: {str(e)}")
            raise

        self.LL = 0
        self.HL = 90
        time.sleep(0.1)

    def get_config(self, axis_type=['R', 'R', 'X'], auto=False):
        ans = self.ask("GSI")
        print("System ID:", ans)
        ans = self.ask("GNC")
        print("Number of channels:", ans)
        numchan = int(ans[1:])
        self.numchan = numchan
        # allocate N axis properties here

        # get channel type (effector vs. positioner ?)
        for chan in range(numchan):
            ans = self.ask("GCT{}".format(chan))
            print("channel ", chan, " is", ans)

        # get sensor type
        for chan in range(numchan):
            ans = self.ask("GST{}".format(chan))
            print("sensor ", chan, " is", ans)
        # Set sensor type for vacuum motors
        if axis_type[2] == 'T':
            self.ask('SST2,1')
        # Enable sensors (all channels)

        # check if the sensors are enabled
        ans = self.ask("GSE")
        print("Sensors  enabled:", ans)

        # sensors: 9,9,9,16,2,1
        # 1 means none

        # axis 4 does not respond, Ill try to change the encoder type.
        print('Configuring the axis...')
        if auto == False:
            self.axis_type = axis_type  # confiugure all rotations
        # the following routine can be used if I have all the motors with sensors
        elif auto == True:
            self.axis_type = []
            for chan in range(numchan):
                self.axis_type.append("X")  # unknown/not defined
            for chan in range(numchan):
                ans = self.ask("GA{}".format(chan))
                if self.error == 0:
                    print(
                    "Channel", chan, "is rotation")
                    print(
                    "  Angle position ", chan, " is", ans)
                    self.axis_type[chan] = "R"
                else:
                    self.axis_type[chan] = "T"
                # this block is useful only if we have translation stage with sensor
                # for chan in range(numchan):
                ans = self.ask("GP{}".format(chan))
                if self.error == 0:
                    print(
                    "Channel", chan, "is translation")
                    print(
                    "  Linear position ", chan, " is", ans)
                    self.axis_type[chan] = "T"
                    # if the motion is rot, than GP returns error and other way arround. This way I can get the type of the motor

        print("axis type:", self.axis_type)

    def axis_config(self, name, number, type, sensor):
        # for now everything seems to be already configured
        return

    def send(self, msg):
        self.error = 0
        if self.proto == "RS232":
            m = ":%s\n" % msg
        if self.proto == "TCPIP":
            m = (":%s\n" % msg).encode('utf-8')
            logging.debug(f'>>> {m}')
            self.s.send(m)

    def read(self):
        if self.proto == "RS232":
            data = self.s.read(1024)
        if self.proto == "TCPIP":
            d = self.s.recv(1024)
            logging.debug(f'<<< {d}')
            data = d.decode('utf-8')
        # print data
        if (data[1] == 'E'):
            # this still does not mean that this is an error, it could ne the eooer 0, which is OK
            ErrNum = int(data[1:-1].split(",")[1])
            if not ErrNum == 0:
                self.error = 1
                logging.error(f"Read ERROR: {ErrNum}")
                if ErrNum in self.errorcode.keys():
                    logging.error(f"...this means: {self.errorcode[ErrNum]}")

        return data[1:-1]  # this removes ":" and "\n"

    def ask(self, string):
        self.send(string)
        # FIXME: this should be async / await for read()
        return self.read()


    async def async_ask(self, string):
        self.send(string)
        # FIXME: this should be async / await for read().
        # To do this, we need to set the entire socket timeout to 0
        # (and possibly use select()), and we should only do this
        # when there's no more interefernce with non-async read().
        # (...or non-async read() is prepared to handle a zero-timeout socket.)
        return self.read()


    def asknum(self, string):
        self.s.flush()
        self.send(string)
        time.sleep(0.05)
        ans = self.read()
        # print "string answer = ",ans
        listans = ans.split(",")[1]
        # print " list answer = ",listans
        numans = float(listans)
        # print "numeric answer = ",numans
        return numans

    async def async_asknum(self, string):
        retry = True
        while retry:
            try:
                self.send(string)
                ans = self.read()
                # print "string answer = ",ans
                listans = ans.split(",")[1]
                # print " list answer = ",listans
                numans = float(listans)
                retry = False
                # print "numeric answer = ",numans
            except:
                continue
        return numans

    def reset(self):
        print(
        "Resetting, next answer is :E-1,0")
        self.send("R")
        time.sleep(3)
        self.read()
        print(
        "resetted")
        self.get_config()
        # Xlab trouble, trying to force the encoder type
        # set the sensor type
        # ans = self.ask("SST4,2")
        # calibrate sensor
        # ans = self.ask("CS4")
        time.sleep(5)



    def reset_channel(self, chan):
        if self.axis_type[chan] == "R" or self.axis_type[chan] == "T":
            print("resetting channel", chan)

            ans = self.asknum("GPPK{}".format(chan))
            print(
            " position active:", ans)
            if ans == 0:
                print(
                "  position not active, resetting")
                ans = self.ask("SSD{},0".format(chan))
                ans = self.ask("GSD{}".format(chan))
                print(
                "  safe dir", ans)
                # ans = self.ask("CS{}".format(chan))
                # sleep(2)
                # print "calibrate",ans
                time.sleep(1)
                ans = self.ask("FRM{},0,1000,1".format(chan))
                print(
                "  zero result", ans)
                time.sleep(3)
                print(
                "waiting")
                #self.wait(chan)

        ans = self.asknum("GPPK{}".format(chan))
        print(
        " position active:", ans)
        if ans == 0:
            print(
            "  trying in other direction")
            ans = self.ask("FRM{},1,1000,1".format(chan))
            print(
            "   zero result", ans)
            time.sleep(3)
            #self.wait(chan)

        ans = self.asknum("GPPK{}".format(chan))
        print(
        " position active:", ans)
        if ans == 0:
            print(
            "  position not active, changing safe direction")
            ans = self.ask("SSD{},1".format(chan))
            ans = self.ask("GSD{}".format(chan))
            print(
            "  safe dir", ans)
            # print "  calibration"
            # ans = self.ask("CS{}".format(chan))
            # time.sleep(2)
            # print "   calibrate",ans
            ans = self.ask("FRM{},1,1000,1".format(chan))
            print(
            "  zero result", ans)
            self.get_status()
            time.sleep(3)
            #self.wait(chan)
        ans = self.asknum("GPPK{}".format(chan))
        print(
        " position active:", ans)
        if ans == 0:
            print(
            "##################################################")
            print(
            "  WARNING")
            print(
            "  calibration on axis ", chan, " did not work!")
            print(
            "  Probably wont move")
            print(
            "##################################################")

    def recalibrate_all(self):
        # this tries to see if the axes are active and drives them to limit switch
        for chan in range(numchan):
            self.reset_chanel(numchan)
        # sets low speed
        for chan in range(numchan):
            self.set_speed(chan, 1000)

        # set encoder types:
        # the are already set, mo need to change

        # now move relative, just for fun:
        for chan in range(numchan):
            print(
            "moving channel", chan)
            if self.axis_type[chan] == "R":
                # ans = self.ask("SSD{},1".format(chan))
                ans = self.ask("MAA{},3000000,0,1000".format(chan))
                print("  move angle", ans)
            if self.axis_type[chan] == "T":
                ans = self.ask("MPA{},-3000000,1000".format(chan))
                print("  move position", ans)
                # ans = self.ask("CS{}".format(chan))
            time.sleep(1)

        for chan in range(numchan):
            print(
            "moving channel", chan)
            if self.axis_type[chan] == "T":
                ans = self.ask("GP{}".format(chan))
                print("Channel", chan, "is translation")
                print("  Linear position ", chan, " is", ans)
            if self.axis_type[chan] == "R":
                ans = self.ask("GA{}".format(chan))
                print("Channel", chan, "is rotation")
                print("  Angle position ", chan, " is", ans)

        self.numchan = numchan
        ans = self.ask("MAA4,-3000000,0,1000")
        time.sleep(1)
        ans = self.ask("MAA4,3000000,0,1000")
        time.sleep(1)
        ans = self.ask("MAA4,-3000000,0,1000")
        time.sleep(1)

    def calibrate(self, axis=-1):
        # this is not neccessary for our motor type
        # setting safe direction
        if axis > -1:
            chan = axis
            print(
            "Channel", chan)
            # print "setting safe direction"
            # self.ask("SSD{},1".format(chan))
            ans = self.ask("GSD{}".format(chan))
            print("safe direction=", ans)
            # calibrate the sensor
            ans = self.ask(f'CS{chan}')
            print("calibrating returned", ans)

        else:
            for chan in range(self.numchan):
                print(
                "Channel", chan)
                # print "setting safe direction"
                # self.ask("SSD{},1".format(chan))
                self.ask("GSD{}".format(chan))
                # calibrate the sensor
                self.ask("CS{}".format(chan))

    def print_status(self):
        status = 0
        for chan in range(self.numchan):
            if self.axis_type[chan] == "R" or self.axis_type[chan] == "T":
                ans = self.asknum("GS{}".format(chan))
                status = status + ans
                print("status chan", chan, ":", ans,)
                if ans == 0:
                    print(
                    "Stopped")
                if ans == 1:
                    print(
                    "Stepping")
                if ans == 2:
                    print(
                    "Scanning")
                if ans == 3:
                    print(
                    "Holding")
                if ans == 4:
                    print(
                    "Targetting (closed loop moving)")
                if ans == 5:
                    print(
                    "Move Delay (waiting after move)")
                if ans == 6:
                    print(
                    "Calibrating")
                if ans == 7:
                    print(
                    "Finding Reference Mark")
                if ans == 8:
                    print(
                    "This should not happen")
                if ans == 9:
                    print(
                    "Locked")
        return status

    async def async_get_status(self,chan):
        status = 0
        if self.axis_type[chan] == "R" or self.axis_type[chan] == "T":
            ans = await self.async_asknum("GS{}".format(chan))
            status = status + ans
        return status

    async def wait_for_stop(self,chan):
        state = None
        while (state is None) or (state > 0):
            logging.info(f'Staus {state}')
            state = await self.async_get_status(chan)
            await asyncio.sleep(0.1)

    async def async_get_position(self, chan):
        ans = None
        retry = True
        if self.axis_type[chan] == "R":
            # FIXME: should be async here
            while retry:
                try:
                    ans = await self.async_ask(f'GA{chan}')
                    if chan == 1:
                        ans = 'This motor has no sensor'
                    elif chan == 2:
                        ans = 'This motor does not exists'
                    elif chan == 0:
                        logging.debug(f'GetPosition answer: {ans}')
                        angle = float(ans.split(",")[1]) / 1000000.0
                # circle = float(ans.split(",")[2])
                # print " list answer = ",listans
                        ans = angle
                        retry = False
                except: 
                    continue

        if self.axis_type[chan] == "T":
            # FIXME: should be async here
            ans = await self.async_asknum("GP{}".format(chan)) / 1000000
        return ans

    async def async_vac_move(self, chan, position, wait = False):
        if chan == 0 :
            if position > 0:
                await self.async_ask(f'MST{chan},{int(42500/90*position)},1200,1000')
            else:
                await self.async_ask(f'MST{chan},{int(40000/90*position)},1200,1000')
        else:
            logging.info('Only Channel 0 is supported by this funciton')
        

    async def async_move(self, chan, position, wait=False):
        # in this version the decision on how to move a motor
        # is made on axis number chan, if I have all motors with sensors
        # I can decide based on the axis type..
        logging.debug(f'Motion requested on axis {chan}, {type}, {self.axis_type[chan]}')
        if chan == 0:
            logging.debug(f'You requested a motion on axis 0, the motion is absolute '
                          f'and the position in degrees')
            circle = 0
            if position >= 0:
                if position >= 450:
                    logging.error(f'Forbidden to rotate more than 45 degrees')
                    return
                ans = self.send("MAA{},{},0,1000".format(chan, int(position * 1000000)))
                logging.debug(ans)

            else:
                if position <= -450:
                    logging.error('Forbidden to rotate more than 45 degrees')
                    return
                position = 360 + position
                circle = -1
                await self.async_ask("MAA{},{},{},100".format(chan, int(position * 1000000), circle))
        elif chan == 1:
            logging.debug(f'You requested a motion on axis 1, the motion '
                          f'is relative and the position in unknown units')
            await self.async_ask("MST{},{},4095,1000".format(chan, int(position)))
        elif chan == 2:
            logging.debug('You requested a motion on axis 2, the motion is absolute I guess')
            await self.async_ask("MPA{},{},0".format(chan, int(position * 1000000)))

        if wait:
            logging.info('Waiting...')
            await self.wait_for_stop(chan)


    async def async_start_homing(self, chan):
        ret = await self.async_ask(f'FRM{chan},0,1,0')
        logging.info(f'Homing return value: {ret}')


    def set_position(self, chan, pos):
        if chan == 0:
            self.ask("SP{},{}".format(chan, int(pos * 1000000)))
            print(
            "Motor on axis {} setted to position {}".format(chan, pos))
        else:
            print('The reset is possible only for axis 0')

    def set_velocity(self, axis, speed):
        if speed < 50:
            speed = 50
        if speed > 3000:
            speed = 18000
        self.ask("SCLF{},{}".format(axis, speed))
        # SCLF0,3000

class USBController:
    def __init__(self,UsbID):
        self.d_handle= ctl.Open(UsbID)
        self.ctl=ctl

    def get_config(self):
        device_sn = ctl.GetProperty_s(self.d_handle, 0, ctl.Property.DEVICE_SERIAL_NUMBER)
        logging.info("MCS2 device serial number: {}".format(device_sn))
        self.numchan = ctl.GetProperty_i32(self.d_handle, 0, ctl.Property.NUMBER_OF_CHANNELS)
        logging.info("MCS2 number of channels: {}.".format(self.numchan))
        self.base_unit=[]
        for channel in range(self.numchan):
            state = ctl.GetProperty_i32(self.d_handle, channel, ctl.Property.CHANNEL_STATE)
            self.base_unit.append(ctl.GetProperty_i32(self.d_handle, channel, ctl.Property.POS_BASE_UNIT))
            if state & ctl.ChannelState.SENSOR_PRESENT:
                ctl.MoveMode(0)
                if self.base_unit[-1] ==ctl.BaseUnit.METER:
                    logging.info(f'MCS2 channel {channel} has a linear Stage with sensor.')
                else:
                    print(f'MCS2 channel {channel} has a rotational Stage with sensor.')
            else:
                print(f'MCS2 channel {channel} has no sensor.')
        return self.base_unit

    def set_velocity(self,chan,velo):
        ctl.SetProperty_i64(self.d_handle, chan, ctl.Property.MOVE_VELOCITY, int(velo*1e9))
        return

    def get_status(self,chan):
        status = ctl.GetProperty_i32(self.d_handle, chan, ctl.Property.CHANNEL_STATE)
        return status

    def get_position(self, chan):
        ans = ctl.GetProperty_i64(self.d_handle, chan, ctl.Property.POSITION)
        return ans/1e9

    def move(self, chan, position):
        ctl.Move(self.d_handle,chan,int(position*1e9))
        while ctl.GetProperty_i32(self.d_handle, chan, ctl.Property.CHANNEL_STATE) != 262242:
            time.sleep(0.15)
        return

class SmarActStage(Motor):
    def __init__(self,Controller,Name,Channel,LowerLimit,UpperLimit,Unit='unit', vac = False):
        super().__init__()
        self.controller = Controller
        self.units = Unit
        self.chan = Channel
        self.min = LowerLimit
        self.name = Name
        self.max = UpperLimit
        self.vac = vac
        self.hwPosition = 0
        #self.updatePosition()

    async def initialize(self):
        await self.asyncUpdatePosition()

    #def receivePositionFromController(self):
    #    self.hwPosition = self.controller.get_position(self.chan)
    #    return self.hwPosition

    async def asyncReceivePositionFromController(self):
        if self.vac:
            return self.hwPosition
        else:
            self.hwPosition = await self.controller.async_get_position(self.chan)
            return self.hwPosition

    async def asyncSendPositionToController(self, new_pos):
        if self.vac:
            relativeMovement = new_pos-self.hwPosition
            self.hwPosition += relativeMovement
            return await self.controller.async_vac_move(self.chan,relativeMovement)
        else:
            return await self.controller.async_move(self.chan, new_pos)

    async def read_controller_state(self):
        return await self.controller.async_get_status(self.chan)

    async def asyncHome(self):
        '''
        Triggers a homing motion
        '''
        return await self.controller.async_start_homing(self.chan)

    def set_velocity(self, velocity):
        self.controller.set_velocity(self.chan, velocity)
        print('Velocity Set to %.1f' % velocity)

