#!/usr/bin/python3

import numpy as np
import time
import threading

# from Machine import state, BusyException, States
from PyQt5.QtCore import QRunnable, QObject, pyqtSignal


class Motor(QObject):
    motorPositionChanged = pyqtSignal()

    def __init__(self,):
        super().__init__()
        self.controller = None
        self.name = "motor"
        self.units = "unit"
        self.hwPosition = 0
        self.userPosition = 0
        self.offset = 0
        self.min = 0
        self.max = 0
        self.invert = False
        self.state = 0
        self.destination = 0
        self.history = []
        self.checkInvert()

    # To be implemented by the subclass
    def receivePositionFromController(self):
        return self.hwPosition

    # To be implemented by the subclass
    async def asyncSendPositionToController(self, position):
        self.hwPosition = position

    # To be implemented by the subclass
    def receiveStateFromController(self):
        self.checkArrival()

    # To be implemented by the subclass
    # Returns True if motion stopped and False if not possible
    def sendStopToController(self):
        return False

    # To be implemented by the subclass
    def sendHomeToController(self, position):
        print('Homing not included yet!')
        return

    def home(self):
        self.state = 1
        self.sendHomeToController()
        return True

    def checkInvert(self):
        if self.invert:
            self.min= self.max*(-1)
            self.max= self.min*(-1)

    async def asyncUpdatePosition(self):
        self.hwPosition = await self.asyncReceivePositionFromController()
        if self.invert:
            self.userPosition =  self.offset - self.hwPosition
        else:
            self.userPosition = self.hwPosition + self.offset
        self.motorPositionChanged.emit()
        return


    def updatePosition(self):
        self.hwPosition = self.receivePositionFromController()
        if self.invert:
            self.userPosition =  self.offset - self.hwPosition
        else:
            self.userPosition = self.hwPosition + self.offset

        self.motorPositionChanged.emit()
        return


    async def asyncSetHWPosition(self, position):
        await self.asyncSendPositionToController(position)
        # self.state = 1
        # self.destination = position
        # th = threading.Thread(target = self.sendPositionToController, args=(position,),daemon=True)
        # th.start()
        # self.updatePosition()

    def checkArrival(self):
        self.updatePosition()
        if np.isclose(self.hwPosition,self.destination, rtol=1e-3):
            self.state = 0

    def waitForReady(self):
        while self.state == 1:
            self.checkArrival()

    async def asyncSetUserPosition(self, position):
        if self.invert:
            position = position*(-1)
        newHWPosition = position - self.offset
        if newHWPosition < self.min or newHWPosition > self.max:
            logging.error(f"ERROR: Attempt to set {self.name} hardware position to {newHWPosition}, while limits are ({self.min}"
                          f", {self.max})")
            return False
        await self.asyncSetHWPosition(newHWPosition)
        return True

    def setRelativeUserPosition(self, distance):
        new_pos = self.userPosition + distance
        self.setUserPosition(new_pos)

    def setOffset(self, offset):
        self.offset = offset
        self.updatePosition()

    def zeroUserPosition(self):
        self.offset = -self.hwPosition
        self.updatePosition()

    async def asyncGetUserPosition(self):
        await self.asyncUpdatePosition()
        return self.userPosition

    def getState(self):
        self.receiveStateFromController()
        return self.state

    def stop(self):
        self.state = -1
        if self.sendStopToController():
            self.state = 0
        return

class TestMotor(Motor):
    def __init__(self):
        super().__init__()
        self.name = "test_motor"
        self.units = "deg"
        self.min = 0
        self.max = 90

    async def receivePositionFromController(self) -> float:
        return self.hwPosition

    def receivePositionFromController(self) -> float:
        return self.hwPosition

    def sendPositionToController(self, position: float):
        time.sleep(2)
        self.hwPosition = position
