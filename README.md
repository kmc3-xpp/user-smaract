SmarAct
=======

Top-level controller script (and container) for a SmarAct MCS controller
(stage control). This is used at KMC3 by the X-ray Optics Group of IKZ Berlin.

This is designed to run on `bespin` ad KMC3, typically like this:
```
  git clone https://gitlab.com/kmc3-xpp/user-smaract
  podman build -t smaract-ioc -f ./smaract/Dockerfile
  podman run -ti --rm --net=host smaract-ioc
```

Note that this is contains heavily modified copies of `SmarAct.py`
and `Motor/Motor.py`, slightly modified, possibly not yet ported
upstream. (Upstream project with the X-ray Optics Group is at\
https://github.com/). Modifications were made for usage enhancements
(i.e. configuration through environment variables), but also
for fixing / synchronizing crucial communications issues and making
the device EPICS-capable.

Environment variables for configuration:

 - `SMARACT_AIR_CTRL_HOST`: used by `SmarAct.py/IPController`,
   defaults to 192.168.1.25. At KMC3, likely 172.16.58.201.

 - `SMARACT_VAC_CTRL_HOST`: defaults to 192.168.1.200, at KMC3
   likely 172.16.58.200.


 - `SMARACT_EPICS_PREFIX`: EPICS prefix to use

 - `SMARACT_POLL_PERIOD`: period with which to poll device status
   and current position, in seconds. Defaults to 0.2 seconds.

There are two sets of EPICS PV suffixes exported: one for
stage `VAC` and one for `AIR`. Here the suffixes for each:
 
 - `THETA`: the angle setter PV
 - `THETA_RBV`: angle read-back value
 - `DMOV`: done moving (turns 0 when stage moves, 1 when it doesn't)
 - `HOME`: if set to 1, executes a homing movement and resets to 0 when done
 - `STATUS`: numerical status of the stage, as received through the
   controller. Typically, this 0 for "stopped" and several values
   of non-zero corresponding to activities "step", "scan", "hold", "move" (5?),
   "calibrate".

